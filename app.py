from flask import Flask, request, jsonify, render_template
from request import parse_data
from  datetime import date, datetime, timedelta
from pymemcache.client import base
import ast
from request import code,arr
app = Flask("app")

code.append('AZN')



@app.route('/', methods=['GET','POST'])
def home_page():
    context = {
        'code': code
    }

    if request.method == 'POST':
        value = request.form.get('value')
        currency1 = request.form.get('currency1')
        currency2 = request.form.get('currency2')

        if currency1 != 'AZN' and currency2!='AZN':
            val1 = list(filter(lambda x: x['code'] == currency1, arr))
            azn = float(value) *val1[0]['value']['$']
            val2 = list(filter(lambda x: x['code'] == currency2, arr))
            cur2 = azn / val2[0]['value']['$']

            context['result'] = cur2
        elif currency1 == 'AZN':
            val2 = list(filter(lambda x: x['code'] == currency2, arr))
            cur2 = int(value) / val2[0]['value']['$']

            context['result'] = cur2

        elif currency2 == 'AZN':
            val1 = list(filter(lambda x: x['code'] == currency1, arr))
            azn = float(value) * val1[0]['value']['$']

            context['result'] = azn

    return render_template('new_index.html',context=context)



# @app.route('/test')
# def test():
#     return render_template('new_index.html')

app.run(debug=True, host='0.0.0.0', port=8000)